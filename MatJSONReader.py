################################################################
# DT Tran
# Last update: 3/27/2013
# Just like the first version, but updated the way to detect 
# end of asets section and add getAnnots(...)
# Last update: 4/3/2013
# Just like the 2nd version, but add ability to find non json file 
# given a folder: find_non_json(folder)
# Note: assume the file in question is a MatJSON file format
# 1/4/2014: change to use MatJSONDocument object, can skip this and use
# MatJSONDocument directly
################################################################

import json, os
import csv
import re
from operator import itemgetter
from os import listdir
from os.path import isfile, join

from MatJSONDocument import *
from Annotation import *

def find_non_json(folder):
    """returns a file name in the folder that is not a json format"""
    filesToRead = [ f for f in listdir(folder) if isfile(join(folder,f)) ]
    for filepath in filesToRead:
        data = open(os.path.join(folder, filepath), 'rb').read()
        try:
            jsonData = json.loads(data)
        except:
            print filepath + 'is not json'
            raise


def get_asets(file):
    """returns a dictionary of asets in this MatJSON file"""

    doc = MatJSONDocument(file)
    
    return doc.asets

def count_asets(fileName, asetType = None):
    """return a count of aset of asetType or all aset type if asetType is None"""
    
    typeCountDict = {}

    asetsDictionary = get_asets(fileName)

    """if asetType is not None, returns a count for that specific type"""
    if (asetType is not None):
        if asetType in asetsDictionary.keys:
            return len(asetsDictionary[asetType])
        else:
            return 0
    else:
        #returns count for all aset type
        for type in asetsDictionary:
            typeCountDict[type] = len(asetsDictionary[type])

        return typeCountDict


def getContentText(file):
    """get the original document text (between signal and asets) in a MatJSON file format """
    #print file
    doc = MatJSONDocument(file)
    
    return doc.text


def get_asets_texts(fileName, types = []):
    """get a list of the text using the asets offsets where asets are of types[]. not sorted"""

    texts = []
    #get document text part MatJSON file
    contents = getContentText(fileName)
    
    #get a dictionary of the MatJSON asets (all of them)
    dictionary = get_asets(fileName)

    #for each aset type in param
    for t in types:
        if t in dictionary:
            offsets = dictionary[t] #get offsets list for this type
            for offset in offsets:
                #get content from offset start to offset end
                texts.append(contents[offset[0]:offset[1]])

    return texts

def get_asets_sorted_texts(fileName, types = [], sorted= True):
    """get a list of the text using the asets offsets where asets are of types[]. sorted"""

    texts = []
    offsets = []
    #get document text part MatJSON file
    contents = getContentText(fileName)
    
    #get a dictionary of the MatJSON asets (all of them)
    dictionary = get_asets(fileName)

    #for each aset type in param
    for t in types:
        if t in dictionary:
            offsets.extend(dictionary[t])
    
    offsets.sort() #want to sort by offset start, do i have to do something else besides just sort()
    
    for offset in offsets:
        #get content from offset start to offset end
        texts.append(contents[offset[0]:offset[1]])


    return texts

#like get_asets_sorted_texts, but including offset as well as the text span within that offset
def getAnnots(fileName, types, sortByOffset= True):
    """get a list of the text using the asets offsets where asets are of types[]. not sorted"""

    info = []
    offsets = []
    #get document text part MatJSON file
    contents = getContentText(fileName)
    #get a dictionary of the MatJSON asets (all of them)
    dictionary = get_asets(fileName)

    #for each aset type in param
    for t in types:
        if t in dictionary:
            offsets = dictionary[t]
            for offset in offsets:
                #get content from offset start to offset end
                start = offset[0]
                end = offset[1]
                content = contents[start:end]
                #info.append({'label':t, 'start':start, 'end':end, 'content':content})
                info.append(Annotation(start, end, content, t))
            
    if (sortByOffset == True):
        #want to sort by offset start, do i have to do something else besides just sort()
        #can use this syntax if were using #info.append({'label':t, 'start':start, 'end':end, 'content':content}) above
        #info = sorted(info, key=itemgetter('start')) 
        info = sorted(info, key=lambda annot: annot.start)
    


    return info

#different from get_asets_texts because i'm getting the text from another file, not the same MatJson file
def get_asets_texts_inFile(fileWText, fileMatJSON, types = []):
    """get a list of the text in fileText using the asets offsets where asets are of types[]"""

    texts = []
    #get document text in annotation file, no MatJson
    with open(fileWText, 'r') as fileIn:
        contents = fileIn.read()
    fileIn.closed
    
    #have to mimic MIST, put backlash infront of quote in text and before line return
    contents = re.sub('"', r'\\"', contents)
    contents = re.sub('\r\n', r'\\r\\n', contents)

    #get a dictionary of the MatJSON asets (all of them)
    dictionary = get_asets(fileMatJSON)

    #for each aset type in param
    for t in types:
        if t in dictionary:
            offsets = dictionary[t] #get offsets list for this type
            for offset in offsets:
                #get content from offset start to offset end
                texts.append(contents[offset[0]:offset[1]])

    return texts

def count_total_asets(fileName, types = []):
    """count the total number of asets of types[]"""

    #gets a dictionary of asets count for all asets
    dictionary = count_asets(fileName)

    count = 0
    for t in types:
        if t in dictionary:
            count += dictionary[t]

    return count


def print_count_asets(matJsonFolder, outFile, types = []):
    """print a count of asets to a folder and file name where asets are of types[] """

    #reads all the MatJSON files in a folder, write to an outFile the count of asets per annotation type
    #types specifies only the list of aset types to count
    
    with open(outFile, 'wb') as file:
        writer = csv.writer(file, delimiter='\t')
        #write the column headers
        header = ['File']
        header.extend(types)
        writer.writerow(header)
       

        for filename in os.listdir(matJsonFolder):
            dictionary = count_asets(matJsonFolder + filename)
            countsPerType = []
            try:
                for type in types:
                    if type in dictionary:
                        countsPerType.append(str(dictionary[type]))
                    else:
                        countsPerType.append('0')
            except:
                raise

            countsPerType.insert(0, filename)
            writer.writerow(countsPerType)
            print filename
    file.closed

#dt add 5/24
def getTokenTextAtOffset(tokenList, startOffset = None, endOffset = None):
    """ Return the text between the start position to end position 
    If only the start offset is known, then return the token text that starts there
    If only the end offset is known, then return the token text that ends there
    If both offsets are known, concatinate all the token texts that starts and ends
    at these offsets
    tokenList is a list of Annotations
    
    """
    textAtOffsets = ''
    if startOffset != None and endOffset == None:
        for t in tokenList:
            if t.start == startOffset:
                return t.content
    if startOffset == None and endOffset != None:
        for t in tokenList:
            if t.end == endOffset:
                return t.content
    if startOffset != None and endOffset != None:
        for t in tokenList:
            if t.start >= startOffset and t.end <= endOffset:
                textAtOffsets += t.content + ' ' #added space 2/21/2014 when realized space is not capture in tokenList
        return textAtOffsets[0:-1]

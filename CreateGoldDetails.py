################################################################
# DT Tran
# Created to combine known PII leaks and surrogates
# into a file call gold_details.csv
################################################################

import csv
import sys
import getopt
import os.path

from MatJSONReader import *
from MATExperimentDetail import *



types = ['org_name','pt_name','doc_name','date','other_id'
         ,'address','phone','age','email','ip_addr','url'
         ,'med_rec_num','ssn','room','doc_initials']

goldFile = "gold_details.csv"

def main(argv):
    """Creates a gold_details.csv file, including all known PII
    (leaked PII and surrogates)

    Keyword arguments:
    --details is the details.csv file from a MIST experiment run
    --ref is the directory of the original annotated mat-json files
    --hyp is the dir. of the experiment engine's output mat-json files
    --surr is the dir. of the clear to clear surrogatized mat-json files
    --out is the dir. to create the gold_details.csv file
    --list is a file that lists the file names to be included in this
    gold_details file. (optional)

    IMPORTANT: examine the gold_details.csv for mentions of "****WRONG****".
    This means something didn't align correctly.
    Possible cause: '00123456' that gets saved by Excel as '123456'
    Also check for error messages on the console
    """

    try:
        opts, args = getopt.getopt(argv, "",
                    ["details=", "ref=", "surr=", "out=", "list=", "hyp=", "help" ])
    except getopt.GetoptError:
        print main.__doc__
        sys.exit(2)
        
    outDir = ''
    detailsFile = '' 
    documentList = ''
    referenceFolder = ''
    hypotheticalFolder = ''
    surrogatizedFolder = ''
    
    # this is the calculated PII leaks and surrogates
    goldDetails = []

    for opt, arg in opts:
        if opt in ("--details"):
            detailsFile = arg
        elif opt in ("--ref"):
            referenceFolder = arg
        elif opt in ("--hyp"):
            hypotheticalFolder = arg
        elif opt in ("--surr"):
            surrogatizedFolder = arg
        elif opt in ("--out"):
            outDir = arg
        elif opt in ("--list"):
            documentList = arg
        elif opt in ("--help"):
            print main.__doc__
            sys.exit()
    
    if (outDir == '' or detailsFile == '' or referenceFolder == '' 
        or hypotheticalFolder == '' or surrogatizedFolder == ''):
        print "Missing at least one required parameter"
        print main.__doc__
        sys.exit()

    outFile = os.path.join(outDir, goldFile)
    

    goldDetails = getLeaksAndSurrogates(detailsFile, referenceFolder
                                        , hypotheticalFolder
                                        , surrogatizedFolder, documentList)
    if os.path.exists(outFile):
        raise IOError("%s already exist" % (outFile))

    writeGoldDetails(outFile, goldDetails)
    
    

def getLeaksAndSurrogates(detailsCsv, refFolder, hypFolder, surrFolder
                          , documentList=''):
    """Returns a list of MATExperimentDetail objects

    For each row in the details.csv file
    if MIST found a PII, record the surrogate for that PII instance
    if MIST did not find a PII, record the original PII for that instance
    if MIST only find part of the PII, record the original and the surrogate part
    Save the information to a MATExperimentDetail object
    
    """
    # each item in details[] is a row in the details.csv file
    details = []

    # each file name is a key for a list of deid results in the order
    # that they appear in the details.csv file
    # example: {'filename':[detail1,detail2,detail3,...]}
    detailsByFile = {}
    
    # list of document names to be read from the detailsCsv file
    # this is useful when we ran the experiment engine on many documents but 
    # we only want to create the gold details for a subset of that corpus
    docList = []

    docList = get_docList(documentList)
    detailsByFile = get_details(detailsCsv, docList)

    # text of the matjson surrogatized file being processed
    surrogateFileText = ''

    # surrogates[] stores a list of surrogates that MIST previously created
    surrogates = []

    # stores the calculated PII leaks and surrogates info
    goldDetails = []

    # process each row in the details file in the order that it was saved
    # it's only logical that the details are sorted by file name, and the rows
    # are in the order that they would appear in the document
    # (sorted by the start offset)

    #refactor 2/18/2014 to have access to prev, curr, next row
    #mainly because there are many cases with overmark, then spurious
    for filename in detailsByFile:
        details = detailsByFile[filename]

        originalTokens = getAnnots(hypFolder + '\\' + filename, ['lex'])
		#Find before ".prepped" to make this work -Rachel
        fileNameWNoPrefix = filename[0: filename.find('.prepped')]
        surrogateFileText = getContentText(surrFolder + '\\' + filename)
        surrogates = getAnnots(surrFolder + '\\' + filename, types)
        references = getAnnots(refFolder + '\\' + fileNameWNoPrefix, types)
        hypotheticals = getAnnots(hypFolder+ '\\' + filename, types)

        detailsMngr = MATExperimentGoldDetailsManager(filename, details, originalTokens, references, hypotheticals, surrogates, surrogateFileText)
        #print filename
        detailsMngr.process()
        goldDetails.extend(detailsMngr.goldDetails)
        
    return goldDetails

def writeGoldDetails(outFile, goldDetails):
    """Write out the leaks and surrogates to a gold_details.csv file"""
    
    goldDetailsFile = outFile

    csv.register_dialect('escapedtab', escapechar = ",",quotechar="'"
                         ,doublequote = True, quoting=csv.QUOTE_ALL
                         , delimiter ='\t')
    csv.register_dialect('escapedcomma', escapechar = '"',quotechar='"'
                         ,doublequote = True, quoting=csv.QUOTE_ALL
                         , delimiter =',')
    csv.register_dialect('singlequote',quotechar="'", doublequote = True
                         , quoting = csv.QUOTE_ALL)
    countFile = 0
    try:
        with open(goldDetailsFile, 'wb') as fileOut:
            writer = csv.writer(fileOut,  )
            writer.writerow(['file','type','reflabel','hyplabel','refstart','refend',
                             'hypstart','hypend','refcontent','hypcontent',
                             'surlabel','surstart','surend', 'surcontent',
                             'goldlabel','goldstart','goldend', 'goldcontent'
                             , 'cumulativeOffsetChange'])
        
            for detail in goldDetails:
                writer.writerow([detail.filename 
                                , detail.type 
                                , detail.reflabel 
                                , detail.hyplabel 
                                , detail.refstart 
                                , detail.refend 
                                , detail.hypstart 
                                , detail.hypend 
                                , quoteString(detail.refcontent) 
                                , quoteString(detail.hypcontent)
                                , detail.surlabel
                                , detail.surstart
                                , detail.surend
                                , quoteString(detail.surcontent)
                                , detail.goldlabel 
                                , detail.goldstart 
                                , detail.goldend 
                                , quoteString(detail.goldcontent)
                                , detail.offsetChange])
                countFile += 1
    except Exception as e:
        print "Error: fail to write out a PII instance to the gold_details.csv"
    finally:
        print "Had %d PII instances, wrote out %d instances" % (len(goldDetails), countFile)

def getRefOverlapHypToken(refStart, refEnd, refContent, refLabel, hypTokens):
    """Return an overlap instance of PII found by MIST if there was one, or None"""
    
    for t in hypTokens:
        if refStart >= t.start and refEnd <= t.end:
            return t
        else:
            continue

    return None


def isOverlap(rowA, rowB):
    """Return whether refs of rowA envelops hyp of rowB are MATExperimentDetail object"""
    if rowA.refstart == '' or rowB.hypstart == '':
        return False
    if int(rowA.refstart) <= int(rowB.hypstart) and int(rowA.refend) >= int(rowB.hypend):
        return True
    else:
        return False

def get_docList(documentList=''):
    """Return the list of documents in this file, or an empty list"""
    
    list = []
    if documentList != '':
        with open(documentList, 'rb') as f:
            list = [elem for elem in f.read().split() if elem] 

    return list

def get_details(detailsFile, documentList=[]):
    """ Return the details of this file as a dictionary list of MATExperimentDetail
    if there was any, or the subset list of files specified in the documentList,
    or an empty list. the filename is the key
    
    """
    details = {}
    with open(detailsFile, 'rb') as f:
        reader = csv.reader(f)
        rowNumber = 0

        if len(documentList) == 0:
            #do not need to reduce to this specific documentList
            for row in reader:
                rowNumber += 1
                #skips the row header
                if rowNumber > 1:
                    filename = row[1]
                    if filename not in details:
                        details[filename] = []

                    details[filename].append(MATExperimentDetail
                                   (row[1], row[2], row[7], row[8],  row[9], 
                                    row[10], row[11], row[12],row[13], row[14]))
        elif len(documentList) >0:
            #need to check if filename in detailsFile is on approved documentList
            for row in reader:
                rowNumber += 1
                if rowNumber > 1 and row[1] in documentList:
                    filename = row[1]
                    if filename not in details:
                        details[filename] = []

                    details[filename].append(MATExperimentDetail
                                   (row[1], row[2], row[7], row[8],  row[9], 
                                    row[10], row[11], row[12],row[13], row[14]))

    return details

def quoteString(text):
    """Quote text to fix the issue with Excel display issue"""
    if(len(text) > 0):
        return '"'+ str(text) + '"'

if __name__ == "__main__":
    main(sys.argv[1:])

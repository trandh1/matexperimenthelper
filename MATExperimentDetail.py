################################################################
# DT Tran
# MATExperimentDetail: Created to hold information for each row in gold_details.csv
# MATExperimentGoldDetailsManager: to create custom gold_details.csv
# because the details.csv that MIST produces sometimes splits spanclashes as overmark + spurious or undermark + missing
# This version addresses the basic span clashes the way we like, but occasionally 
# the span clashes can be very complicated and still needs to be reviewed.
################################################################
import sys
import re
from Annotation import *
from MatJSONReader import *


class MATExperimentDetail():
    """a row on the MATExperimentEngine gold_details.csv file with important elements"""

    """ I can force the offset to be 0 if the cell is blank, 
    but then it looks like hypstart is 0 when it's blank so I choose not to store it as an int"""

    def __init__(self, filename, ptype='', reflabel='', hyplabel='', refstart='', refend='',
                 hypstart='', hypend='', refcontent='', hypcontent='',
                 surlabel='', surstart='', surend='', surcontent='',
                 goldlabel='', goldstart='', goldend='', goldcontent='', offsetChange=''):
        self.filename = filename
        self.type = ptype
        self.reflabel = reflabel
        self.hyplabel = hyplabel
        self.refstart = refstart
        self.refend = refend
        self.hypstart = hypstart
        self.hypend = hypend
        self.refcontent = refcontent
        self.hypcontent = hypcontent

        self.surlabel = surlabel
        self.surstart = surstart
        self.surend = surend
        self.surcontent = surcontent

        self.goldlabel = goldlabel
        self.goldstart = goldstart
        self.goldend = goldend
        self.goldcontent = goldcontent
        self.offsetChange = offsetChange
        

        try:
            self.goldend_i_ = int(self.goldend)
            self.goldstart_i_ = int(self.goldstart)
        except ValueError:
            self.goldend_i_ = None
            self.goldstart_i_ = None
        try:
            self.surend_i_ = int(self.surend)
            self.surstart_i_ = int(self.surstart)
        except ValueError:
            self.surend_i_ = None
            self.surstart_i_ = None

    def has_type(self, ptype):
        return self.type.find(ptype) >= 0

    def get_surstart(self):
        return self.surstart_i_

    def get_goldstart(self):
        return self.goldstart_i_

    def get_goldend(self):
        return self.goldend_i_

    def get_surend(self):
        return self.surend_i_

    def getFrontLeakOffsets(self):
        """Return the offsets of a partial leak that occurs at the beginning of a PII"""
        if self.get_surstart() > self.get_goldstart():
            return self.get_goldstart(), self.get_surstart()
        return None

    def getTailLeakOffsets(self):
        """Return the offsets of a partial leak that occurs at the end of a PII"""
        if self.get_surend() < self.get_goldend():
            return self.get_surend(), self.get_goldend()
        return None

    def isOvermark(self):
        return self.has_type('overmark')

    def isUndermark(self):
        return self.has_type('undermark')

    def isOverlap(self):
        return self.has_type('overlap')

    def isTagclash(self):
        return self.has_type('tagclash')

    def isMatch(self):
        return self.type == 'match'

    def isSpurious(self):
        return self.type == 'spurious'

    def isMissing(self):
        return self.type == 'missing'

    def setDetail(self, ref, hyp, surr, gold, ptype='', offset=0):
        if ref:
            self.reflabel = ref.label
            self.refstart = str(ref.start)
            self.refend = str(ref.end)
            self.refcontent = ref.content
        else:
            self.reflabel = ''
            self.refstart = ''
            self.refend = ''
            self.refcontent = ''

        if hyp:
            self.hyplabel = hyp.label
            self.hypstart = str(hyp.start)
            self.hypend = str(hyp.end)
            self.hypcontent = hyp.content
        else:
            self.hyplabel = ''
            self.hypstart = ''
            self.hypend = ''
            self.hypcontent = ''

        if surr:
            self.surlabel = surr.label
            self.surstart = str(surr.start)
            self.surend = str(surr.end)
            self.surcontent = surr.content
        else:
            self.surlabel = ''
            self.surstart = ''
            self.surend = ''
            self.surcontent = ''

        if gold:
            self.goldlabel = gold.label
            self.goldstart = str(gold.start)
            self.goldend = str(gold.end)
            self.goldcontent = gold.content
        else:
            self.goldlabel = ''
            self.goldstart = ''
            self.goldend = ''
            self.goldcontent = ''

        self.type = ptype
        self.offsetChange = offset


class MATExperimentGoldDetailsManager():
    """manage a list of MATExperimentDetails and compile a list of gold details"""


    def __init__(self, filename, details, originalTokens, references, hypotheticals, surrogates, surrogateFileText):
        self.filename = filename
        self.details = details
        self.originalTokens = originalTokens
        self.references = references
        self.hypotheticals = hypotheticals
        self.surrogates = surrogates
        self.surrogateFileText = surrogateFileText
        self.goldDetails = []
        self.cumulativeOffsetChange = 0
        #Create a list to store all used offsets -Rachel
        self.offsetList = [];

    def process(self):
        """the main method to crunch through a list of experiment details for this file 
            and returns a list of gold details
        """
        if len(self.hypotheticals) != len(self.surrogates):
            # Not always an error, example OR03FP00401D00555.prepped.tag.json overmark, then spurious at the end of the overmark span
            errMsg = "Warning: surrogates has " + str(len(self.surrogates)) + " for " + str(
                len(self.hypotheticals)) + " MIST annotations in " + self.filename
            i = 0
            #while i < len(self.surrogates):
            #     print self.surrogates[i].content
            #     i = i+1
            print errMsg

        while len(self.references) > 0:
            if len(self.hypotheticals) > 0:
                ref = self.references[0]
                hyp = self.hypotheticals[0]
                if ref.end < hyp.start:
                     self.processFullLeak()
                else:
                    #if self.filename == "ds-doc_699.json.prepped.tag.json":
                    #     print self.filename, ' before undermark ', self.references[0].content, ' ', self.references[0].end, ' ',  self.hypotheticals[0].content, ' ', self.hypotheticals[0].start

                     self.processNonFullLeak()
                    #if self.filename == "ds-doc_699.json.prepped.tag.json":
                    #     print self.filename, ' after undermark ', self.references[0].content, ' ', self.references[0].start, ' ', self.references[0].end, ' ',  self.hypotheticals[0].content, ' ', self.hypotheticals[0].start, ' ', self.hypotheticals[0].end
            else:
                # len(self.hypotheticals) is 0 but there are still references
                # they are all missing/leaks that MIST didn't get a hypothetical hit
                 self.processFullLeak()

        if len(self.references) > 0: raise Exception("did not account for all leaks")

        # in case there are more hypothetical left over after all references are considered
        # or may be there was 0 references
        while len(self.hypotheticals) > 0:
            # they are all spurious that wasn't in original gold
            self.processSpurious()

        if len(self.hypotheticals) > 0: raise Exception("did not account for all spurious pii")
        if False and len(self.surrogates) > 0: raise Exception("did not account for all surrogates")

    def processFullLeak(self):
        """helper method to process(), use to create a 'missing' experiment detail
        in a full leak, the reference is not match by a hypothetical, so there is no surrogate
        and the adjudicated gold is just the reference. Remove the leak instance from references
        """

        if len(self.references) > 0:
            detail = MATExperimentDetail(self.filename)

            ref = self.references.pop(0)
            hyp = None
            surr = None
            gold = Annotation()
            ref.copyTo(gold)
            gold.start += self.cumulativeOffsetChange
            gold.end = gold.start + gold.length
            resultType = 'missing'
            #if self.filename == "ds-doc_699.json.prepped.tag.json":
            #     print self.filename, ' full leak ', self.references[0].content, ' ', self.references[0].start, ' ', self.references[0].end
 
            self.addToGoldDetails(detail, ref, hyp, surr, gold, resultType)  # does not need surOverlapLen
        else:
            raise Exception('Expecting to process a full leak but there is no leak item to process in ' + self.filename)

    def processFullSpanMatch(self):
        """helper method to process(), create a 'match' or 'tagclash' experiment detail
        in a full span match, the reference and the hypothetical offsets are the same
        but the label could be different for a tagclash. Remove the ref and hyp pair from their lists"""
        #if self.filename == "ds-doc_699.json.prepped.tag.json":
        #    print self.filename, ' in match '
        if len(self.references) > 0 and len(self.hypotheticals) > 0:
            ref = self.references[0]
            hyp = self.hypotheticals[0]
            # double check that this method is appropriate for this pair
            #print ref.content, hyp.content
            if True or (ref.hasSameContent(hyp)):
                detail = MATExperimentDetail(self.filename)

                self.references.pop(0)
                self.hypotheticals.pop(0)
                #if self.filename == "ds-doc_699.json.prepped.tag.json":
                #     print self.filename, ref.content, ' ', hyp.content, ' '
                if len(self.surrogates) > 0:
                    surr = self.surrogates.pop(0)
                    gold = surr
                    if ref.label != hyp.label:
                         resultType = 'tagclash'
                    else:
                         resultType = 'match'
					#If ref and surr have different labels, surr is not the right match for ref/hyp.
                    #Push surr back to the list of surrogates. -Rachel
                    if ref.label != surr.label and resultType == 'match':
                         #print self.filename, ' Not Matching Surrogate: ', ref.label, ' ', ref.content, ' ', surr.label, ' ', surr.content
                         self.surrogates[0] = surr
                         return
                    self.addToGoldDetails(detail, ref, hyp, surr, gold, resultType)

            else:
                raise Exception('Expecting a full span match')
        else:
            raise Exception(
                'Expecting to process a match or tagclash but there is no ref and hyp left in ' + self.filename)

    def processSpurious(self):
        """helper method to process(), create a 'spurious' experiment detail,
        MIST annotated a hypothetical instance not in the ref, so MIST surrogatized it. 
        Remove the hyp and surr from their lists
        """
        if len(self.hypotheticals) > 0 and len(self.surrogates) > 0:
            detail = MATExperimentDetail(self.filename)
            ref = None
            hyp = self.hypotheticals.pop(0)
            #print self.filename
            surr = self.surrogates.pop(0)
            gold = surr
            resultType = 'spurious'
            self.addToGoldDetails(detail, ref, hyp, surr, gold, resultType)
        else:
            print 'no surrogate left for Spurious row in ', self.filename, 'hyp: ', self.hypotheticals[0].content
            self.hypotheticals.pop(0)
            #raise Exception(
            #    'Expecting to process a spurious row but there is no surrogate left in ' + self.filename)

    def processOvermarks(self):
        """helper method to process(), keep a single as overmark or create match/tag clash
        , or combine overmark with spurious or keep them separate
        """
        if len(self.references) == 0 or len(self.hypotheticals) == 0:
            raise Exception(
                'Expecting to process an overmark but there is no valid ref or hyp left in ' + self.filename)

        # remove current hyp and see if current ref span also covers the next hyp
        hyp = self.hypotheticals[0]
        ref = self.references.pop(0)

        if not ref.isOvermark(hyp):
            raise Exception(
                'Expecting to process an overmark but ref and hyp is not an overmark pair in ' + self.filename)
        #if self.filename == "ds-doc_699.json.prepped.tag.json":
        #     print self.filename, ' overmark 2: ', ref.content, ' ', hyp.content
        overmarks = ref.findNextOverlap(self.hypotheticals)
        while len(overmarks) > 0:
            # resets
            hyp = self.hypotheticals[0]
            extra = ''
            hypEndToNextHyp = 0
            leakLen = 0
            realLeak = False
            #if self.filename == "ds-doc_699.json.prepped.tag.json":
            #     print self.filename, ' overmark 2: ', ref.content, ' ', hyp.content
            if len(overmarks) == 1:
                # single overmark
				#overmark means part of the ref is in hyp -Rachel
                if ((ref.start < hyp.start)# and ref.end == hyp.end)
                    or (ref.end > hyp.end)):#(ref.start == hyp.start and ref.end > hyp.end)):
                    surr = self.getSurrogates()
                    detail = MATExperimentDetail(self.filename)
                    # gold needs to contain the leaked part
                    gold = Annotation()
                    surr.copyTo(gold)
					#Process when hyp is in the middle of ref -Rachel
                    if ref.start < hyp.start and ref.end > hyp.end:
					#Quick hack to make sure gold starts from the correct position, fixed hp_doc-98 -Rachel
					#Check if there's overlap between the end of ref and the start of surr, if so, split the overlapping part from gold
                        leakLen1 = hyp.start - ref.start
                        string = ref.content[:leakLen1]
                        match  = re.findall(r'[^\s.]+', string)
                        if len(match) > 0: 
                           last_match = match[len(match)-1]
                           print last_match
                           print gold.content.find(last_match)
                           if gold.content.find(last_match) != -1:
                              gold.content = gold.content[len(last_match):]
                              gold.start += len(last_match)
                        #if self.filename == 'hp-doc_98.json.prepped.tag.json':
                        #   print ref.start, ' ', ref.end, ' ', ref.content, ' ', hyp.start, ' ', hyp.end, ' ', hyp.content, ' ', self.filename

                        #if self.filename == 'hp-doc_98.json.prepped.tag.json':
                        #   print '1: ', gold.start, ' ', gold.end, ' ', gold.content, ' ', surr.content, ' ', hyp.start, ' ', hyp.end, ' ', ref.content[:(leakLen1+ hyp.end - ref.start)] ,  ' ', self.filename
                        gold.start -= leakLen1
                        gold.content = ref.content[:leakLen1] + gold.content
                        #if self.filename == 'hp-doc_98.json.prepped.tag.json':
                        #   print '2: ', gold.start, ' ', gold.end, ' ', gold.content, ' ', self.filename
                        leakLen2 = ref.end - hyp.end
                        gold.end += leakLen2
                        gold.content += ref.content[-leakLen2:]
                        #if self.filename == 'hp-doc_98.json.prepped.tag.json':
                        #   print '3: ', gold.start, ' ', gold.end, ' ', gold.content, ' ', self.surrogateFileText[gold.start:gold.end], ' ', self.filename
                        if gold.content != self.surrogateFileText[gold.start:gold.end]:
                           surr.copyTo(gold)
                           self.references.insert(0, ref)
                           print '4: ', ref.content, ' ', gold.content, ' smaller'
                           self.surrogates.insert(0,surr)						   
                           self.processFullSpanMatch()
                           #print self.references[0].content, ' ', self.hypotheticals[0].content

                           overmarks = ref.findNextOverlap(self.hypotheticals)
                           continue	
					#Process when hyp overlaps with the end of ref -Rachel
                    elif ref.start < hyp.start:				   
                        leakLen = hyp.start - ref.start
                        gold.start -= leakLen
                        gold.content = ref.content[:leakLen] + gold.content
						#check to see if the modified gold.content is the same with surr, if not, roll back to surr 
						#This happens when the supposed leak part by overmark is actually changed by an adjacent tag -Rachel
                        if gold.content != self.surrogateFileText[gold.start:gold.end]:
                           surr.copyTo(gold)
                           self.references.insert(0, ref)
                           print ref.content, ' ', gold.content, ' smaller'
                           self.surrogates.insert(0,surr)						   
                           self.processFullSpanMatch()
                           #print self.references[0].content, ' ', self.hypotheticals[0].content

                           overmarks = ref.findNextOverlap(self.hypotheticals)
                           continue						   
					#Process when hyp overlaps with the start of ref -Rachel
                    else:
                        #print gold.content
                        #print ref.content
                        leakLen = ref.end - hyp.end
                        gold.end += leakLen
                        gold.content += ref.content[-leakLen:]
                        #print gold.content
						#check to see if the modified gold.content is the same with surr, if not, roll back to surr 
						#This happens when the supposed leak part by overmark is actually changed by an adjacent tag -Rachel
                        if gold.content != self.surrogateFileText[gold.start:gold.end]:
                           surr.copyTo(gold)
                           self.references.insert(0, ref)
                           print ref.content, ' ', gold.content, ' bigger' 
                           self.surrogates.insert(0,surr)	
                           self.processFullSpanMatch()
                           overmarks = ref.findNextOverlap(self.hypotheticals)
                           continue						   

                    resultType = 'overmark'
                    if ref.label != hyp.label: resultType += ',tagclash'
                    self.addToGoldDetails(detail, ref, hyp, surr, gold, resultType)
                    self.hypotheticals.pop(0)
                elif ref.start == hyp.start and ref.end == hyp.end:
                    self.references.insert(0, ref)
                    self.processFullSpanMatch()
                else:
                    #print ref.start,' ',ref.end,' ',ref.content,' ',hyp.start,' ',hyp.end,' ',hyp.content
                    raise Exception('Unexpected case in ' + self.filename)
            else:
                if ref.start != hyp.start:
                    if False:  # todo
                        raise Exception(
                            'Unexpected gap in ' + self.filename + ' ' + str(hyp.start) + ' ' + str(ref.start))
                nextHyp = self.hypotheticals[1]
                hypEndToNextHyp = nextHyp.start - hyp.end
                if hypEndToNextHyp > 1:  # gap 
                    # get content within that gap
                    extra = ref.content[len(hyp.content):len(hyp.content) + hypEndToNextHyp]
                    print ref.content[len(hyp.content) - 10:len(hyp.content) + hypEndToNextHyp + 10]
                    print nextHyp.start, hyp.end
                    print "extra::", extra
                    if re.search('[a-zA-Z0-9]', extra):  # TODO: may be add more to this regex\
                        realLeak = True
                        print 'Warning, gap contains real character in ' + self.filename
                if realLeak:
                    tempRef = Annotation(ref.start, hyp.end + len(extra)
                                         , ref.content[0:len(hyp.content) + len(extra)], ref.label)
                    self.references.insert(0, tempRef)
                    # self.processFullSpanMatch()
                    self.processOvermarks()
                    ref.start = hyp.end + len(extra)
                    ref.content = ref.content[len(hyp.content) + len(extra):]
                elif hyp.label != nextHyp.label or re.search(' ', hyp.content) or re.search(' ', nextHyp.content):
                    # split up ref and create match/tagclash for hyp'
                    tempRef = Annotation(ref.start, hyp.end, ref.content[0:len(hyp.content)], ref.label)
                    if self.filename == "doc_173.json.prepped.tag.json":
                         print self.filename, ' tempRef: ', tempRef.content
                    self.references.insert(0, tempRef)
                    self.processFullSpanMatch()
                    ref.start = hyp.end + len(extra)
                    ref.content = ref.content[len(hyp.content) + len(extra):]
                else:
                    # if (0 <= hypEndToNextHyp <= 2): #tolerance of 2 characters
                    hyp.end = nextHyp.end
					#added a space -Rachel
                    hyp.content += extra + nextHyp.content
                    self.hypotheticals.pop(1)
                    surr = self.getSurrogates()
                    nextSurr = self.getSurrogates()

                    if surr and nextSurr:
                        surr.end = nextSurr.end
						#added a space in between -Rachel
 
                        if ref.content.find(" ") > 0:
                           surr.content += extra + ' ' + nextSurr.content
                        else:
                           surr.content += extra + nextSurr.content 
                           #print len(hyp.content), ' ', len(surr.content),' ',len(nextSurr.content)
                        if self.filename == "doc_173.json.prepped.tag.json":
                           print self.filename, ' ref: ', ref.content, ' lenRef: ', len(ref.content), ' surr: ', surr.content, ' nextSurr: ', nextSurr.content
                        #print 'hyp = ', hyp.content, 'nestHyp = ',nextHyp.content 
                        #print 'extra = ', extra, "next = ", nextSurr.content
                    if surr:
                        self.surrogates.insert(0, surr)
                        # else:
                        #    #split up ref and create match/tagclash for hyp
                        #    tempRef = Annotation(ref.start, hyp.end, ref.content[0:len(hyp.content)], ref.label)
                        #    self.references.insert(0, tempRef)
                        #    self.processFullSpanMatch()
                        #    ref.start = hyp.end
                        #    ref.content = ref.content[len(hyp.content):]

            # update overmarks to check while look condition
            overmarks = ref.findNextOverlap(self.hypotheticals)

    def processUndermarks(self):
        """helper method to process(), combine undermark with missing or keep as undermark"""

        if len(self.references) == 0 or len(self.hypotheticals) == 0:
            raise Exception(
                'Expecting to process an undermark but there is no valid ref or hyp left in ' + self.filename)

        # remove current hyp and see if current hyp span also covers the next ref
        hyp = self.hypotheticals.pop(0)
        ref = self.references[0]

        if not ref.isUndermark(hyp):
            raise Exception(
                'Expecting to process an undermark but ref and hyp is not an undermark pair in ' + self.filename)

        undermarks = hyp.findNextOverlap(self.references)
        while len(undermarks) > 0:
            # resets
            ref = self.references[0]
            extra = ''
            refEndToNextRef = 0
            if len(undermarks) == 1:
                # single undermark
				#undermark means part of the hyp is in ref -Rachel
                if (ref.end < hyp.end or ref.start > hyp.start):#(ref.start == hyp.start and ref.end < hyp.end)
                    #or (ref.start > hyp.start and ref.end == hyp.end)
                    #or (ref.start > hyp.start and ref.end < hyp.end)):
                    surr = self.getSurrogates()
                    detail = MATExperimentDetail(self.filename)
                    gold = surr
                    resultType = 'undermark'
                    if ref.label != hyp.label: resultType += ',tagclash'
                    #if self.filename == "ds-doc_699.json.prepped.tag.json":
                    #     print self.filename, 'undermark: 1', ref.content, ' ', hyp.content, ' ', surr.content
                    self.addToGoldDetails(detail, ref, hyp, surr, gold, resultType)
                    self.references.pop(0)
                elif ref.start == hyp.start and ref.end == hyp.end:
                    self.hypotheticals.insert(0, hyp)
                    self.processFullSpanMatch()
                else:
                    raise Exception('Unexpected case in ' + self.filename)
            else:
                nextRef = self.references[1]
                if (ref.start < hyp.start or ref.end > hyp.end
                    or nextRef.start < hyp.start or nextRef.end > hyp.end):
                    raise Exception('Unexpected gap in ' + self.filename)

                # get gap between ref end to next ref start
                refEndToNextRef = nextRef.start - ref.end
                if refEndToNextRef > 0:
                    # extra = hyp.content[len(ref.content):len(ref.content)+refEndToNextRef]
                    gapStart = ref.end - hyp.start
                    extra = hyp.content[gapStart: gapStart + refEndToNextRef]
                # next ref ends before or at hyp ends, update ref to covers next ref and remove next ref
                if ref.end < nextRef.end:
                    ref.end = nextRef.end
                    if refEndToNextRef >= 0:
                        ref.content += extra + nextRef.content
                    else:
                        ref.content += nextRef.content[-(nextRef.end - ref.end):]
                self.references.pop(1)


                # update undermarks to check while look condition
            #if self.filename == "ds-doc_699.json.prepped.tag.json":
            #    print self.filename, 'undermark 5 added: ', ref.content, ' ', hyp.content, ' ', surr.content
            undermarks = hyp.findNextOverlap(self.references)
            #if self.filename == "ds-doc_699.json.prepped.tag.json":
            #    print self.filename, 'undermark 6 added: ', ref.content, ' ', hyp.content, ' ', surr.content
    def processOverlaps(self):
        """helper method to process(), use to split overlap"""

        if len(self.references) == 0 or len(self.hypotheticals) == 0:
            raise Exception('Expecting to process overlap pair but there is no ref or hyp left in ' + self.filename)
        ref = self.references.pop(0)
        hyp = self.hypotheticals[0]
        #if self.filename == "ds-doc_699.json.prepped.tag.json":
        #     print self.filename, 'overlap: '
        if ref.start < hyp.start and ref.end > hyp.start and ref.end < hyp.end:
            # overlap case: save 2 rows with missing/leak in front then undermark'
            overlapLen = hyp.start - ref.start
            newRef = Annotation(ref.start, hyp.start, ref.content[0:overlapLen], ref.label)
            self.references.insert(0, newRef)
            self.processFullLeak()
            #if self.filename == "ds-doc_699.json.prepped.tag.json":
            #     print self.filename, 'overlap: 2'
            newRef = Annotation(hyp.start, ref.end, ref.content[overlapLen:], ref.label)
            self.references.insert(0, newRef)
            #if self.filename == "doc_182.json.prepped.tag.json":
			#	print self.filename, ' ', ref.content, ' ', hyp.content, ' ', ref.label
			#overmarks? -Rachel
            self.processUndermarks()
            #if self.filename == "ds-doc_699.json.prepped.tag.json":
            #     print self.filename, 'undermark 7 added: '

        elif ref.start > hyp.start and ref.start < hyp.end and ref.end > hyp.end:
            # overlap case: save 2 rows with undermark then missing/leak'
            overlapLen = hyp.end - ref.start
            newRef = Annotation(ref.start, hyp.end, ref.content[0:overlapLen], ref.label)
            self.references.insert(0, newRef)
			#overmarks? -Rachel
            #if self.filename == "doc_182.json.prepped.tag.json":
			#	print self.filename, ' ', len(self.hypotheticals)#self.hypotheticals[1].content, ' ',len(self.hypotheticals[1].content), ' ', len(ref.content[overlapLen:])
            self.processUndermarks()
            #if self.filename == "ds-doc_699.json.prepped.tag.json":
            #     print self.filename, 'undermark 8 added: '

			#only add missing when the rest of ref is not in following hyp -Rachel
            if ref.content[overlapLen:].find(self.hypotheticals[0].content) == -1:
			#if len(self.hypotheticals) == 1 or (len(self.hypotheticals) > 1 and ref.content[overlapLen:].find(self.hypotheticals[1].content) == -1):
                #if self.filename == "doc_182.json.prepped.tag.json":
				#  print self.filename, ' ', hyp.content, ' ',self.hypotheticals[0].content, ' ', len(ref.content[overlapLen:])
            
                newRef = Annotation(hyp.end, ref.end, ref.content[overlapLen:], ref.label)
                self.references.insert(0, newRef)
                self.processFullLeak() 
			
        else:
            raise Exception('Unexpected case in ' + self.filename)

    def processNonFullLeak(self):
        """helper method to process(), use to decide which experiment detail type 
        that is not 'missing' and route to the right method"""

        if len(self.references) > 0 and len(self.hypotheticals) > 0:
            detail = MATExperimentDetail(self.filename)
            ref = self.references[0]
            hyp = self.hypotheticals[0]
            if ref.isMissing(hyp):
                raise Exception('Not expecting missing type when processing non full leak in file ' + self.filename)
            if ref.start >= hyp.end:

                self.processSpurious()
            elif ref.start == hyp.start and ref.end == hyp.end:
                self.processFullSpanMatch()
            elif ref.isOvermark(hyp):
                #if self.filename == "doc_182.json.prepped.tag.json":
				#   print self.filename, ' ', ref.content, ' ', hyp.content
                self.processOvermarks()
            elif ref.isUndermark(hyp):
                #if self.filename == "doc_182.json.prepped.tag.json":
				#   print self.filename, ' ', ref.content, ' ', hyp.content
                self.processUndermarks()
                #if self.filename == "ds-doc_699.json.prepped.tag.json":
                #     print self.filename, 'undermark 9 added: '

            else:  # probably overlap
                self.processOverlaps()
        else:
            raise Exception(
                'Expecting to process a non leak experiment row but there is no ref or hyp left in ' + self.filename)

    def getSurrogates(self, index=0):
        """return surrogate at index, or next surrogates or None"""
        if len(self.surrogates) > 0 and index < len(self.surrogates):
            return self.surrogates.pop(index)
        else:
            print 'No surrogates available at requessted index in ' + self.filename

    def tupleMatch(a,b):
        return len(a)==len(b) and all(i is None or j is None or i==j for i,j in zip(a,b))

    def tupleCombine(a,b):
        return tuple([i is None and j or i for i,j in zip(a,b)])

    def tupleSearch(findme, haystack):
        return [tupleCombine(findme,h) for h in haystack if tupleMatch(findme, h)]
	
    def addToGoldDetails(self, detail, ref, hyp, surr, gold, resultType, surOverlapLen=0):
        """does this for any type of experiment detail in the gold details list"""
        # verify that gold offsets yield correct gold content
        i = 0
        #if self.filename == "ds-doc_699.json.prepped.tag.json" and resultType == "undermark,tagclash":
        #     print self.filename, 'undermark: 2: ref ', ref.content, ' hyp ', hyp.content, ' surr ', surr.content, ' gold ', gold.content,  ' ', gold.start, ' ', gold.end, ' surrFile ', self.surrogateFileText[gold.start:gold.end],
        if gold.content != self.surrogateFileText[gold.start:gold.end]:
            errMsg = "Error: gold content did not match expected text at offsets for " + self.filename
			#a quick hack to find the correct offsets -Rachel
            if resultType == 'missing': 
                #A special case in hp-doc_290 -Rachel
                if self.filename == "hp-doc_290.json.prepped.tag.json" and gold.content == "WALTER M. MORGAN, III" and ref.start == 4281:
                     gold.start += 1
                     gold.end += 1
                     print self.filename
                goldstart = gold.start
                goldend = gold.end
				#search both forward and backward to make sure the match one is in the correct place -Rachel
				#if not found, it is possible that part of the missing instance is changed
                goldforstart = gold.start
                goldforend = gold.end
                goldbacstart = gold.start
                goldbacend = gold.end
                #i = 0
				
				#The outer while loop continues until gold.content matches surrogate file -Rachel
                while gold.content != self.surrogateFileText[goldforstart:goldforend] and gold.content != self.surrogateFileText[goldbacstart:goldbacend] and i != -1: 
                  while gold.end < len(self.surrogateFileText) and gold.content != self.surrogateFileText[gold.start:gold.end]:
                      #if self.filename == 'hp-doc_290.json.prepped.tag.json':
                      #   print self.filename, 'missing: ', gold.content, ' ', gold.start, ' ', gold.end, ' ', self.surrogateFileText[gold.start:gold.end]
                      gold.start += 1
                      gold.end += 1
                  goldforstart = gold.start
                  goldforend = gold.end
				  #Block start -Rachel
                  gold.start = goldstart
                  gold.end = goldend
                  while gold.start > 0 and gold.content != self.surrogateFileText[gold.start:gold.end]:
                        gold.start -= 1
                        gold.end -= 1
                  goldbacstart = gold.start
                  goldbacend = gold.end
				  #if not found any matching, check substring of gold.content in the next loop -Rachel
				  #solved the case when an undermark was followed by a missing, and part of the missing was changed by the undermark. 
				  #e.g., doc-cc-200000-1.61803399.json -Rachel
                  if self.filename == "hp-doc_398.json.prepped.tag.json":
                     print self.filename, ' gold.content: ', gold.content, ' ', self.surrogateFileText[goldforstart:goldforend],' ', self.surrogateFileText[goldbacstart:goldbacend] 
                  existingBac = False
                  existingFor = False
                  if (goldbacstart in (x[0] for x in self.offsetList) or goldbacend in (x[1] for x in self.offsetList)) and gold.content == self.surrogateFileText[goldbacstart:goldbacend]:
                         print "Existing offsets when searching back"
                   #if (goldbacstart, goldbacend) in self.offsetList:
                         existingBac = True
                  elif (goldforstart in (x[0] for x in self.offsetList) or goldforend in (x[1] for x in self.offsetList)) and gold.content == self.surrogateFileText[goldforstart:goldforend]:
                         existingFor = True
                         if self.filename == "hp-doc_398.json.prepped.tag.json":
                             print self.filename, ' gold.content.for: ', gold.content, ' ', self.surrogateFileText[goldforstart:goldforend]

                  else:
                        for x in self.offsetList:
                          #print x
                            if goldbacstart > x[0] and goldbacend < x[1] and gold.content == self.surrogateFileText[goldbacstart:goldbacend]:
                                 print "Existing offsets when searching back"
                                 existingBac = True
                                 break
                      
                  if (existingBac == True and existingFor == True) or (gold.content != self.surrogateFileText[goldforstart:goldforend] and gold.content != self.surrogateFileText[goldbacstart:goldbacend]):
                        #leakLen1 = hyp.start - ref.start
                        #string = gold.content[:leakLen1]                
                      i+=1
                      match  = re.findall(r'[^\s]+', gold.content)
                      numMat = len(match)
                      #if self.filename == "ds-doc_290.json.prepped.tag.json":
                      #   print self.filename, 'missing: 1: ref ', ref.content, ' gold ', gold.content, ' ', numMat

                      if numMat > 0:
                          if i == numMat:
                                 i = -1
                                 return
                          gold.start = goldstart
                          gold.end = goldend    
                          last_match = match[numMat - 1] 
						   #This for loop keeeps track of the last few substrings segmented by whitespace in gold.content -Rachel
                          for j in range(numMat - 1, i - 1, -1):						
                              if j > 0 and j < numMat - 1:
                                 last_match = match[j] + ' ' + last_match
                          #if self.filename == "ds-doc_699.json.prepped.tag.json":
                           #      print self.filename, ' last_match: ', last_match, ' ', i , ' ', j, ' ', numMat
                           #print gold.content.find(last_match)
						   #Reassign all variables to continue the next loop -Rachel
                          gold.content = last_match
                          gold.start = gold.end - len(last_match)
                          ref.content = last_match
                          ref.start = ref.end - len(last_match)
                          goldforstart = gold.start
                          goldforend = gold.end
                          goldbacstart = gold.start
                          goldbacend = gold.end

                           #if self.surrogateFileText[goldbacstart:goldbacend] == last_match:
                           #   gold.content = last_match
                           #   gold.start = goldbacstart
                           #   gold.end = goldbacend
                           #   ref.content = last_match
                           #   ref.start = goldbacstart
                           #   ref.end = goldbacend 
	  
				  #if there're two matches for the missing annotation, check if the first one is in use, if not, 
				  #go with the first one, otherwise go with the second -Rachel	
				  # i==0 means the whole gold.content was found, but this might not be necessary...-Rachel
                if i == 0:				  
                  if goldbacstart == 0 and goldforend < len(self.surrogateFileText):
                         gold.start = goldforstart
                         gold.end = goldforend
                  if goldforend == len(self.surrogateFileText) and goldbacstart > 0:
                         gold.start = goldbacstart
                         gold.end = goldbacend
			        #make sure current offsets are not in existing offset list, neither are part of the offsets. -Rachel
                  if goldforend < len(self.surrogateFileText) and goldbacstart > 0:
                         if goldbacstart in (x[0] for x in self.offsetList) or goldbacend in (x[1] for x in self.offsetList):
                             print "Existing offsets when searching back"
                   #if (goldbacstart, goldbacend) in self.offsetList:
                             gold.start = goldforstart
                             gold.end = goldforend
                         else:
                             found = 0
                             for x in self.offsetList:
                          #print x
                                 if goldbacstart > x[0] and goldbacend < x[1]:
                                     print "Existing offsets when searching back"
                                     found = 1
                                     gold.start = goldforstart
                                     gold.end = goldforend
                                     break
                             if found == 0:
                                 gold.start = goldbacstart
                                 gold.end = goldbacend
                  
				  # Block start -Rachel
                  #if gold.end == len(self.surrogateFileText) and gold.content != self.surrogateFileText[gold.start:gold.end]:
                  #    gold.start = goldstart
                  #    gold.end = goldend
                  #    while gold.start > 0 and gold.content != self.surrogateFileText[gold.start:gold.end]:
                  #        gold.start -= 1
                  #        gold.end -= 1
				  # Block End -Rachel
            else:						
                print errMsg
                print resultType
                print '"{0}"'.format(self.surrogateFileText[gold.start:gold.end])
                print '"{0}"'.format(gold.content)
                if False:
                   raise Exception(errMsg)

        hypLen = 0
        surLen = 0
        if hyp: hypLen = len(hyp.content)
        if surr: surLen = len(surr.content)

        # TODO: make sure this is the right surr and hyp to calculate surOverlapLen.
        if resultType != 'missing':
            surOverlapLen = self.getContentOverlapLength(surr.content, hyp.start, hyp.end)
            #if self.filename == "hp-doc_290.json.prepped.tag.json":
            #     print self.filename, ' surLen: ', surLen, ' ', surr.content, ' hypLen: ', hypLen, ' ', hyp.content
        self.cumulativeOffsetChange += (
            surLen - hypLen - surOverlapLen)  # TODO check to see whether this is a ref variable
			
        #if self.filename == "ds-doc_699.json.prepped.tag.json" and resultType == "undermark,tagclash":
        #     print self.filename, 'undermark: 3: ref ', ref.content, ' hyp ', hyp.content, ' surr ', surr.content, ' gold ', gold.content, ' surrFile ', self.surrogateFileText[gold.start:gold.end]

	    #Add the current gold offsets to offsetList, so that they won't be reused again -Rachel
        self.offsetList.append((gold.start, gold.end))
        detail.setDetail(ref, hyp, surr, gold, resultType, self.cumulativeOffsetChange)
        self.goldDetails.append(detail)
        #if self.filename == "ds-doc_699.json.prepped.tag.json" and resultType == "undermark,tagclash":
        #     print self.filename, 'undermark: 4: ref ', ref.content, ' hyp ', hyp.content, ' surr ', surr.content, ' gold ', gold.content, ' surrFile ', self.surrogateFileText[gold.start:gold.end]

    def getContentOverlapLength(self, contentText, hypStart, hypEnd):
        """Return the length of the overlap content if there was one, or 0"""

        overlapLen = 0

        # find text of token from tokenList where token end == (hypStart - 1)
        textPreHyp = getTokenTextAtOffset(self.originalTokens, endOffset=(hypStart - 1))

        # find text of token from tokenList where token start == hypEnd
        textPostHyp = getTokenTextAtOffset(self.originalTokens, startOffset=hypEnd)

        if textPreHyp != None:
            textSurHead = contentText[0: len(textPreHyp)]
            if self.filename == "hp-doc_290.json.prepped.tag.json":
                 print self.filename, ' textSurHead: ', textSurHead 
            if textPreHyp == textSurHead:
                 print 'surrogate starts with previous text'
                 overlapLen = len(textPreHyp)

        if textPostHyp != None:
            textSurTail = contentText[-len(textPostHyp):]
            if self.filename == "hp-doc_290.json.prepped.tag.json":
                 print self.filename, ' textSurTail: ', textSurTail 
            if textSurTail == textPostHyp:
                 print 'surrogate ends with following text ', self.filename
                 overlapLen += len(textPostHyp)
        if self.filename == "hp-doc_290.json.prepped.tag.json":
             print self.filename, ' overlapLen: ', overlapLen 
        return overlapLen

  
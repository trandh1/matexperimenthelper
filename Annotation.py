################################################################
# DT Tran
# Created to hold information for an annotation
################################################################
class Annotation(object):
    """description of class"""

    def __init__(self, start=-1, end=-1, content='', label=''):
        self.start = int(start)
        self.end = int(end)
        self.content = content
        self.label = label
        self.length = len(content)

    def copyTo(self, toAnnot):
        """copy this object to another Annotation object """
        toAnnot.start = self.start
        toAnnot.end = self.end
        toAnnot.content = self.content
        toAnnot.label = self.label
        toAnnot.length = self.length

    def isMatch(self, compareTo):
        return self.hasSameContent(compareTo) and not self.isTagclash(compareTo) 

    def isTagclash(self, compareTo):
        """if the label doesn't match when they both have real labels, return true"""
        return (self.label != compareTo.label and self.label != '' and compareTo.label != '')
    
    def isTagclashSpanMatch(self, compareTo):
        """if splan overlap and tagclash return true"""
        return self.isTagclash(compareTo) and self.hasSameContent(compareTo)
    
    def hasSameContent(self, compareTo):
        return (self.start == compareTo.start and self.end == compareTo.end
             and self.content == compareTo.content)

    def isOvermark(self, compareTo):
        """if offsets are outside of compareTo, return true"""
        return (self.start <= compareTo.start and self.end >= compareTo.end
                and self.length > compareTo.length)
    
    def isUndermark(self, compareTo):
        """if offsets are inside of compareTo, return true"""
        return (self.start >= compareTo.start and self.end <= compareTo.end
                and self.length < compareTo.length)

    
    def isMissing(self, compareTo):
        """if offsets are valid and compareTo doesn't have a valid offset or compareTo starts after this ends, return true"""
        if compareTo == None or compareTo.start < 0 or compareTo.end < 0:
            return True
        return (self.start >= 0 and self.end >= 0 and compareTo.start >= self.end) 

    def isSpurious(self, compareTo):
        """if offsets are not valid and compareTo does have valid offests, return true"""
        return (compareTo.start >= 0 and compareTo.end >= 0 and (
                (self.start < 0 or self.end < 0) or (self.start >= compareTo.end))
                )
    def isOverlap(self, compareTo):
        """if offsets overlap but not completely within each other"""
        return ((self.start < compareTo.start and self.end > compareTo.start and self.end < compareTo.end)
            or (compareTo.start < self.start and compareTo.end > self.start and compareTo.end < self.end))

    def findNextOverlap(self, potentialMatches):
        """return a list indices in potentialMatches where there is a span overlap. Return an empty list if none exist"""
        returnList = []
        for i, p in enumerate(potentialMatches):
            if (self.isMatch(p) or self.isTagclashSpanMatch(p) or self.isOvermark(p) or self.isUndermark(p)):
                returnList.append(i)
        return returnList
